from tkinter import *
from game_20_48.game2048.grid_2048 import *
from game_20_48.game2048.la_fin_2048 import *
import tkinter.messagebox
from time import *
#一些字典 Some dictionaries
TILES_BG_COLOR = {0: "#9e948a",' ': "#9e948a",2: "#eee4da", 4: "#ede0c8", 8: "#f1b078",
                  16: "#eb8c52", 32: "#f67c5f", 64: "#f65e3b",
                  128: "#edcf72", 256: "#edcc61", 512: "#edc850",
                  1024: "#edc53f", 2048: "#edc22e", 4096: "#5eda92",
                  8192: "#24ba63"}
TILES_FG_COLOR = {0: "#776e65", ' ': "#9e948a",2: "#776e65", 4: "#776e65", 8: "#f9f6f2",
                  16: "#f9f6f2", 32: "#f9f6f2", 64: "#f9f6f2", 128: "#f9f6f2",
                  256: "#f9f6f2", 512: "#f9f6f2", 1024: "#f9f6f2",
                  2048: "#f9f6f2", 4096: "#f9f6f2", 8192: "#f9f6f2"}
TILES_FONT = {"Verdana", 40, "bold"}

THEMES = {"0": {"name": "Default", ' ':"",0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", ' ':"",0: "", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", ' ':"",0: "", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}

#建立初始窗口 Create an initial window
def initial_window():
    global theme_get
    global theme

    initial_win = Tk()
    initial_win.title("2048")
    initial_win.geometry('300x300')
    game_title = Label(initial_win,text="Are You Ready?",
                  fg = "Red",
                  font = ("Times", "30", "bold"))
    game_title.pack()

#选择主题 Choose a theme
    grid_theme_text = Label(initial_win,text="Theme:",fg = "Green",font = ("Times", "18", "bold italic"))
    grid_theme_text.pack()
    theme = IntVar()
    Radiobutton(initial_win,text="Default    ",variable=theme,value=0).pack()
    Radiobutton (initial_win,text="Chemistry",variable=theme,value=1).pack()
    Radiobutton(initial_win,text="Alphabet  ",variable=theme,value=2).pack()

#选择大小 Choose a size
    grid_size_text = Label(initial_win,text="Size:",fg = "Green",font = ("Times", "18", "bold italic"))
    grid_size_text.pack()
    size = StringVar()
    Spinbox(initial_win,values=[i for i in range(2,6)],textvariable = size,width=8,).pack()

#选择时间 Choose the time
    grid_time_text = Label(initial_win,text="Time Limitation:",fg = "Green",font = ("Times", "18", "bold italic"))
    grid_time_text.pack()
    time_limitation = StringVar()
    Spinbox(initial_win,values=[20,40,60],textvariable = time_limitation,width=8,).pack()

#开始和退出按钮 Start and Exit
    start_button = Button(initial_win, text="Start", command=lambda:play_game(int(size.get()),int(theme.get()),int(time_limitation.get())),width=10,fg = "Blue")
    start_button.pack(side=LEFT)
    exit_button = Button(initial_win, text="Exit", command=initial_win.quit,width=10,fg = "Blue")
    exit_button.pack(side=RIGHT)
    theme_get = theme.get()
    initial_win.mainloop()
    return size.get(),theme_get,time_limitation

#移动模块 Movement
#移动模块 Movement
def move(event):
    global game_grid1
    global game_grid2
    global frame_root
    global tl
    global theme_get
    global frame_cell
    global label1
    global label2
    global theme
    if event.char == 'a':
        if move_possible(game_grid1)[0] == True:
            game_grid1 = move_grid(game_grid1, 'left')
            game_grid1 = grid_add_new_tile(game_grid1)
    if event.char == 'd':
        if move_possible(game_grid1)[1] == True:
            game_grid1 = move_grid(game_grid1, 'right')
            game_grid1 = grid_add_new_tile(game_grid1)
    if event.char == 'w':
        if move_possible(game_grid1)[2] == True:
            game_grid1 = move_grid(game_grid1, 'up')
            game_grid1 = grid_add_new_tile(game_grid1)
    if event.char == 's':
        if move_possible(game_grid1)[3] == True:
            game_grid1 = move_grid(game_grid1, 'down')
            game_grid1 = grid_add_new_tile(game_grid1)
    if event.keysym == 'Left':
        if move_possible(game_grid2)[0] == True:
            game_grid2 = move_grid(game_grid2, 'left')
            game_grid2 = grid_add_new_tile(game_grid2)
    if event.keysym == 'Right':
        if move_possible(game_grid2)[1] == True:
            game_grid2 = move_grid(game_grid2, 'right')
            game_grid2 = grid_add_new_tile(game_grid2)
    if event.keysym == 'Up':
        if move_possible(game_grid2)[2] == True:
            game_grid2 = move_grid(game_grid2, 'up')
            game_grid2 = grid_add_new_tile(game_grid2)
    if event.keysym == 'Down':
        if move_possible(game_grid2)[3] == True:
            game_grid2 = move_grid(game_grid2, 'down')
            game_grid2 = grid_add_new_tile(game_grid2)

    for i in range(len(game_grid2)):
        for j in range(len(game_grid2)):
            label2[i][j]['text'] = THEMES[str(theme.get())][game_grid2[i][j]]
            label2[i][j]['bg'] = TILES_BG_COLOR[game_grid2[i][j]]
            label2[i][j]['fg'] = TILES_FG_COLOR[game_grid2[i][j]]
            label2[i][j].grid()
    for i in range(len(game_grid1)):
        for j in range(len(game_grid1)):
            label1[i][j]['text'] = THEMES[str(theme.get())][game_grid1[i][j]]
            label1[i][j]['bg'] = TILES_BG_COLOR[game_grid1[i][j]]
            label1[i][j]['fg'] = TILES_FG_COLOR[game_grid1[i][j]]
            label1[i][j].grid()



class App():
    def __init__(self,time_frame,max_time):
        self.time0=time()
        self.time_noww_copy=0
        self.time_all=max_time
        self.frame=time_frame
        self.label = Label(self.frame,text=0, width=6,height=1,fg = "Red",font = ("Times", "18", "bold italic"))
        self.label.pack()
        self.update_clock(self.time0,self.time_all)

    def update_clock(self,time0,time_all):
        time_now = time()-time0
        self.label['text']=str(time_now)[:4]
        #self.frame.pack()
        #self.tex()
        self.frame.after(1, self.update_clock,time0,time_all)
        self.determinate(time_now,time_all)

    def determinate(self,time_now,time_all):
        if time_now>=time_all:
            self.label['text']='time out!'
            return False
'''
    def surveille(self,time_root,time_noww,time_noww_copy):
        if time_noww!=time_noww_copy:
            time_noww_copy=time_noww
            label=Label(time_root,text=time_noww_copy)
            label.pack()
'''


#展示界面 Show the grid
def graphial_grid(game_grid1,game_grid2,theme_get,time_limitation):
    global frame_root
    global frame_cell
    global label1,label2
    global tl

    tl=Toplevel()
    tl.title('2048')
    frame_root=Frame(tl,borderwidth=5)
    frame_cell = [[0 for i in range(2*len(game_grid1)+1)] for i in range(len(game_grid1)+2)]
    label1 = [[0 for i in range(2*len(game_grid1)+1)] for i in range(len(game_grid1)+2)]
    label2 = [[0 for i in range(2*len(game_grid1)+1)] for i in range(len(game_grid1)+2)]
    for i in range(len(game_grid1)+2):
        for j in range(2*len(game_grid1)+1):
            frame_cell[i][j]=Frame(frame_root,relief=SOLID,borderwidth=1)
            frame_cell[i][j].grid(row=i,column=j)

#展示时间分数 Show the Time and the Scores
    label_time1=Label(frame_cell[0][0],text='Time  1:', width=6,height=1,fg = "Red",font = ("Times", "18", "bold italic"))
    label_time2=Label(frame_cell[0][len(game_grid1)+1],text='Time  2:', width=6,height=1,fg = "Red",font = ("Times", "18", "bold italic"))
    label_score1=Label(frame_cell[1][0],text='Score 1:',width=6,height=1,fg = "Red",font = ("Times", "18", "bold italic"))
    label_score2=Label(frame_cell[1][len(game_grid1)+1],text='Score 2:',width=6,height=1,fg = "Red",font = ("Times", "18", "bold italic"))
    app=App(frame_cell[0][1],time_limitation)
    app=App(frame_cell[0][len(game_grid1)+2],time_limitation)

#制作空白部分 Make the blankspace
    Label(frame_cell[0][len(game_grid1)],text=' ', width=10, height=4).grid()
    for j in range(2*len(game_grid1)+1):
        frame_cell[0][j]['borderwidth']=0
        frame_cell[1][j]['borderwidth']=0

    for i in range(len(game_grid1)+2):
        frame_cell[i][len(game_grid1)]['borderwidth']=0

    for i in range (len(game_grid1)):
        for j in range(len(game_grid1)):
            label1[i][j]=Label(frame_cell[i+2][j],text=THEMES[str(theme_get)][game_grid1[i][j]],bg=TILES_BG_COLOR[game_grid1[i][j]],fg=TILES_FG_COLOR[game_grid1[i][j]],font = ("Times", "30", "bold" ),width = 10,height =4)
            label2[i][j]=Label(frame_cell[i+2][j+1+len(game_grid2)],text=THEMES[str(theme_get)][game_grid2[i][j]],bg=TILES_BG_COLOR[game_grid2[i][j]],fg=TILES_FG_COLOR[game_grid2[i][j]],font = ("Times", "30", "bold" ),width = 10,height =4)
            label1[i][j].grid()
            label2[i][j].grid()

    label_time1.grid()
    label_time2.grid()
    label_score1.grid()
    label_score2.grid()
    frame_root.grid()

#交互 Keyboard
    tl.bind('<Key>', move)
    tl.mainloop()

def play_game(size,theme_get,time_limitation):
    global game_grid1
    game_grid1=init_game(size)
    global game_grid2
    game_grid2=init_game(size)
    graphial_grid(game_grid1,game_grid2,theme_get,time_limitation)

initial_window()
