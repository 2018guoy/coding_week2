# Coding_Week2
2048 （Combat Mode）
Notre jeu permet aux deux joueurs de jouer en même temps en un temps limité.

Fonctionnalité 1: Afficher deux grilles sur la même fenêtre (Zeyuan WANG, Luyang ZHANG)          
(1) Fenêtre initiale: Permettre de choisir le thème, la taille, et le temps limité;
(2) Deux grilles, le temps, les notes: Afficher le temps et la note pour chaque joueur, aussi les deux grilles sur la même fenêtre;
(3) ContrIôle: Joueur 1 utilise W, A, S, D pour contrôler le mouvement de la grille, joueur 2 ↑, ↓, ←, →.

Fonctionnalité 2: Calculer le temps (Zhihao LI, Yin GUO)

Fonctionnalité 3:Enregistrer et afficher la note de chaque joueur(Yin GUO, Luyang ZHANG,Hamza Lamsaoub)
(1)Développer l'algorithme pour calcuer la note de chaque joueur
(2)Montrer le joueur

Fonctionnalité 4: Faire jouer l'ordinateur (Yin GUO)
Permettre au joueur de jouer contre l'ordinateur

Fonctionnalité 5: Black Mode(Zhihao LI, Zeyuan WANG, Hamza Lamsaoub)
Ajouer un nombre noir qu'on peut faire disparaître par mettre deux nombres noirs ensemble