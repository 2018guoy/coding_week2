from game_20_48.game2048.la_fin_2048 import *
def test_is_grid_full():
    assert is_grid_full([[2,9,0],[0,9,9],[1,1,1]]) == False
    assert is_grid_full([[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1]]) == True
#检验网格是否已满


def test_move_possible():
    assert move_possible([[2, 2, 2, 2], [4, 8, 8, 16], [0, 8, 0, 4], [4, 8, 16, 32]]) == [True,True,True,True]
    assert move_possible([[2, 4, 8, 16], [16, 8, 4, 2], [2, 4, 8, 16], [16, 8, 4, 2]]) == [False,False,False,False]

#检验是否存在可能移动
