from game_20_48.game2048.grid_2048 import *
from game_20_48.game2048.textual_2048 import *
import copy
#检验是否已满 If it's full
def is_grid_full(game_grid):
    if get_empty_tiles_positions(game_grid)==[]:
        return True
    else:
        return False

#检验是否可移动 if it's possible to move
def move_possible(game_grid):
    possible =[True, True, True, True]
    grid1 =copy.deepcopy(game_grid)
    grid2 =copy.deepcopy(game_grid)
    grid3 =copy.deepcopy(game_grid)
    grid4 =copy.deepcopy(game_grid)
    if game_grid == move_grid(grid1,"left"):
        possible[0]= False
    if game_grid == move_grid(grid2,"right"):
        possible[1]=False
    if game_grid == move_grid(grid3,"up"):
        possible[2]=False
    if game_grid == move_grid(grid4,"down"):
        possible[3]=False
    return possible

#游戏是否结束 if game over
def is_game_over(game_grid):
    if is_grid_full(game_grid) and move_possible(game_grid)==[False,False,False,False]:
        return True
    else:
        return False

#得到最大值 get the biggest value
def get_grid_tile_max(game_grid):
    return max(get_all_tiles(game_grid))

def win_or_not(game_grid):
    if get_grid_tile_max(game_grid) >= 2048:
        return True
    else:
        return False
