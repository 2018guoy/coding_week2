import random
#建立一个4*4的网格!!!此处有更改，可控制生成网格大小
# Create a grid of the size n*n
def create_grid(n):
    game_grid = []
    for i in range(0,n):
        game_grid.append([' ' for i in range (n)] )
    return game_grid

#在指定位置添加2
#Add "2" to the position (x,y)
def grid_add_new_tile_at_position(game_grid, x, y):
    game_grid[x][y]=2
    return game_grid

#得到全部数字
#get all tiles
def get_all_tiles(game_grid):
    tiles=[]
    for i in range (0,len(game_grid)):
        for j in range(0,len(game_grid[0])):
            if game_grid[i][j]==' ':
                tiles.append(0)
            else:
                tiles.append(game_grid[i][j])
    return tiles

#生成2、4
#get a new value
def get_value_new_tile():
    new_value=random.randint(0,10)
    if new_value==10:
        return 4
    else:
        return 2

#得到全部空位坐标
#get the positions of all the empty tiles
def get_empty_tiles_positions(game_grid):
    empty=[]
    for i in range (0,len(game_grid)):
        for j in range(0,len(game_grid[0])):
            if game_grid[i][j]==' ' or game_grid[i][j]== 0 :
                empty.append((i,j))
    return empty

#得到新的一个空位
#get a new empty tile
def get_new_position(game_grid):
    empty=get_empty_tiles_positions(game_grid)
    get_empty=empty[random.randint(0,len(empty)-1)]
    x,y = get_empty[0],get_empty[1]
    return x,y

def grid_get_value(game_grid,x,y):
    if game_grid[x][y]==' ':
        return 0
    else:
        return game_grid[x][y]

#在任意新空位加入2或4
#add a "2" or a "4" to the new empty tile
def grid_add_new_tile(game_grid):
    x,y = get_new_position(game_grid)
    new_tile = get_value_new_tile()
    game_grid[x][y]= new_tile
    return game_grid

#初始表格
#get the initial grid
def init_game(n):
    game_grid = create_grid(n)
    grid_add_new_tile(game_grid)
    grid_add_new_tile(game_grid)
    return game_grid

#展示初始网格 #show the initial grid
def grid_to_string(game_grid,n):
    equal = " "+"=== "*n
    line = ["|" for k in range(n)]
    for i in range(len(game_grid)):
        for j in range(len(game_grid)):
            line[i] = line[i]+" "+str(game_grid[i][j])+" "+"|"
    string = "\n"+equal
    for k in range(n):
        string = string+"\n"+line[k]+"\n"+equal
    string = string+"\n"
    return string

#得到最大字符长度 get the biggest length
def long_value(game_grid):
    tiles=get_all_tiles(game_grid)
    big_value=max(tiles)
    return len(str(big_value))

#展示初始网格（考虑数字长度 ） show the initial grid with the number and the length
def grid_to_string_with_size(game_grid,n):
    l=long_value(game_grid)
    equal=" "+("="*(l+2)+" ")*n
    line=["|"for k in range(n)]
    for i in range(len(game_grid)):
        for j in range(len(game_grid)):
            line[i] = line[i]+" "+str(game_grid[i][j])+" "*(l+1-len(str(game_grid[i][j])))+"|"
    string = "\n"+equal
    for k in range(n):
        string = string+"\n"+line[k]+"\n"+equal
    string = string+"\n"
    return string

THEMES = {"0": {"name": "Default", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", 0: "", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}
def long_value_with_theme(game_grid,theme):
    if theme["name"]=="Default":
        return long_value(game_grid)
    elif theme["name"]=="Alphabet":
        return 1
    else:
        tiles=get_all_tiles(game_grid)
        if 4 in tiles or 8 in tiles or 16 in tiles or max(tiles)>=1024:
            return 2
        else:
            return 1


#有主题、长度地展示表格 show the grid with the number and the length
def grid_to_string_with_size_and_theme(game_grid,theme,n):
    l=long_value_with_theme(game_grid,theme)
    equal="="+("="*(l+1))*n
    line=["|"for k in range(n)]
    for i in range(len(game_grid)):
        for j in range(len(game_grid)):
            line[i] = line[i]+theme[game_grid[i][j]]+" "*(l-len(theme[game_grid[i][j]]))+"|"
    string = "\n"+equal
    for k in range(n):
        string = string+"\n"+line[k]+"\n"+equal
    string = string+"\n"
    return string


