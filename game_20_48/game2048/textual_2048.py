
#请求一个移动方向 ask for a direction
def read_player_command():
    move = input("Entrez votre commande (g (gauche), d (droite), h (haut), b (bas)):")
    while move not in ['g','d','h','b']:
       move = input("Entrez Entrez votre commande (g (gauche), d (droite), h (haut), b (bas)):")
    return move

def read_size_grid():
    size= int(input("Entrez la taille de la grille:(un entier strictement positif)"))
    while size <=0:
        size= int(input("Entrez la taille de la grille:(un entier strictement positif):"))
    return size

def read_theme_grid():
    theme=input("Entrez le theme du jeu (0 for Default),(1 for Alphabet),(2 for Chemistry):")
    while theme not in ['0','1','2']:
        theme = input("Entrez le theme du jeu (0 for Default),(1 for Alphabet),(2 for Chemistry):")
    return theme

#向左移动 move left
def move_row_left(row):
    l=len(row)
    for i in range(l-1):
        for k in range (i+1,l):
            if row[k]==' ' :
                continue
            if int(row[k])>0:
                if row[i]==' ' :
                    row[i]=row[k]
                    row[k]=0
                elif int(row[i])==0:
                     row[i]=row[k]
                     row[k]=0
                elif int(row[k])== int(row[i]):
                    row[i]=2*int(row[i])
                    row[k]=0
                    break
                else:
                    break
    return row

#向右移动 move right

def move_row_right(row):
    l = len(row)
    for i in range(l-1,0,-1):
        for k in range(i-1,-1,-1):
            if row[k]==' ':
                continue
            if int(row[k])>0:
                if row[i]==' ':
                    row[i] = row[k]
                    row[k] = 0
                elif int(row[i])==0:
                    row[i] = row[k]
                    row[k] = 0
                elif int(row[k]) == row[i]:
                     row[i] = 2*int(row[i])
                     row[k] = 0
                     break
                else:
                     break
    return row

#转置函数 transpose
def transport(game_grid):
    taille = len(game_grid)
    new_grid=[[0 for k in range(taille)] for i in range(taille)]
    for i in range(taille):
        for j in range(taille):
            new_grid[i][j]=game_grid[j][i]
    return new_grid

#向上移动 up
def move_column_up(game_grid):
    new_grid = transport(game_grid)
    for i in range (len(game_grid)):
        new_grid[i] = move_row_left(new_grid[i])
    return transport(new_grid)

#向下移动 down
def move_column_down(game_grid):
    new_grid = transport(game_grid)
    for i in range (len(game_grid)):
        new_grid[i] = move_row_right(new_grid[i])
    return transport(new_grid)

#指定方向移动 move at one direction
def move_grid(game_grid,direction):
    if direction == "left":
        for i in range(len(game_grid)):
            game_grid[i]=move_row_left(game_grid[i])
        return game_grid
    elif direction == "right":
        for i in range(len(game_grid)):
            game_grid[i]=move_row_right(game_grid[i])
        return game_grid
    elif direction == "up":
        return move_column_up(game_grid)
    else:
        return move_column_down(game_grid)
