from game_20_48.game2048.grid_2048 import *
from game_20_48.game2048.la_fin_2048 import *
from game_20_48.game2048.textual_2048 import *
#（1）选择Theme Size choose the theme and the size
THEMES = {"0": {"name": "Default", ' ':"", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", ' ':"", 0: "", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", ' ':"", 0: "", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}
def game_play():
    theme =read_theme_grid()
    size=read_size_grid()

#(2)生成初始表格 Create an initial grid
    game_grid=init_game(size)

#(3)展示初始表格 Show the initial grid
    print(grid_to_string_with_size_and_theme(game_grid,THEMES[str(theme)],size))

#(4)随机选择方向 Choose the direction ()
    dir={1:"left",2:"right",3:"up",4:"down"}
    dir2={'g':1, 'd':2, 'h':3, 'b':4}
    while win_or_not(game_grid) != True:
        while True:
            command=read_player_command()
            number=dir2[command]
            direction= dir[number]
            if move_possible(game_grid)[number-1]==True:
                break
        print(direction)
#(5)移动 Move
        game_grid=move_grid(game_grid,direction)

#(6)展示移动结果 Show the result of the movement
        print(grid_to_string_with_size_and_theme(game_grid,THEMES[str(theme)],size))

#(7)判断是否获胜/结束 If we win or not
        if win_or_not(game_grid):
            print("you win!")
            break
        if is_game_over(game_grid):
            print("Game over!")
            break
        if get_empty_tiles_positions(game_grid)==[]:
            continue

#(8)引入新数 Add a new number
        game_grid=grid_add_new_tile(game_grid)

#(9)展示 Show the new grid
        print(grid_to_string_with_size_and_theme(game_grid,THEMES[str(theme)],size))
        if is_game_over(game_grid):
            print("Game over!")
            break







