from tkinter import *
from time import *
class App():
    def __init__(self,time_root,max_time):
        self.time0=time()
        self.time_all=max_time
        self.root = time_root
        self.frame=Frame(self.root)
        self.label = Label(self.frame,text="1")
        self.label.pack()
        self.update_clock(self.time0,self.time_all)
        self.root.mainloop()

    def update_clock(self,time0,time_all):
        time_now = time()-time0
        self.label["text"]=time_now
        self.frame.pack()
        self.root.after(1000, self.update_clock,time0,time_all)
        self.determinate(time_now,time_all)

    def determinate(self,time_now,time_all):
        if time_now>=time_all:
            while True:
                sleep(1)

root = Tk()
root.title("Countdown")
frame=Frame(root)
app = App(root,10)
root.mainloop()
