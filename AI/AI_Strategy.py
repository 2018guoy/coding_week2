from Time.deux_grilles import *
from random import *
from game_20_48.game2048.la_fin_2048 import *

def choose_primary_and_secondary_direction():
    list_direction=['up','right','down','left']
    primary_direction=choice(list_direction)
    index=list_direction.index(primary_direction)
    list_direction_new=[list_direction[index-1],list_direction[(index+1)%4]]
    secondary_direction=choice(list_direction_new)
    return primary_direction,secondary_direction


def next_move(primary_direction,secondary_direction,game_grid):
    list_direction=['left','right','up','down']
    index_1=list_direction.index(primary_direction)
    index_2=list_direction.index(secondary_direction)
    if index_2==0:
        index_3=1
    elif index_2==1:
        index_3=0
    elif index_2==2:
        index_3=3
    else:
        index_3=2
    third_direction=list_direction[index_3]
    list_direction.remove(primary_direction)
    list_direction.remove(secondary_direction)
    list_direction.remove(third_direction)
    forth_direction=list_direction[0]
    if move_possible(game_grid)[index_1]==True:
        return primary_direction
    elif move_possible(game_grid)[index_2]==True:
        return secondary_direction
    elif move_possible(game_grid)[index_3]==True:
        return third_direction
    else:
        return forth_direction


