from game_20_48.game2048.grid_2048 import *
from math import *

def get_score(game_grid,game_turn):
    game_titles=get_all_tiles(game_grid)
    sum_grid=sum(game_titles)
    raw_score=0.
    for i in range(len(game_titles)):
        if game_titles[i]>0:
            n=log(game_titles[i],2)
            raw_score=raw_score+(n-1)*pow(2,n)
    extra_score=2*(sum_grid-2*(game_turn+2))
    return raw_score-extra_score



